﻿namespace CodeBucket
{
    partial class InfoWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.versionBox = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.infoBox = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.authorBox = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.langBox = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(3, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "Info:";
            // 
            // versionBox
            // 
            this.versionBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.versionBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.versionBox.Location = new System.Drawing.Point(311, 36);
            this.versionBox.Name = "versionBox";
            this.versionBox.Size = new System.Drawing.Size(303, 24);
            this.versionBox.TabIndex = 11;
            this.versionBox.Text = "1.0.0";
            this.versionBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(3, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Version:";
            // 
            // infoBox
            // 
            this.infoBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.infoBox.Location = new System.Drawing.Point(3, 108);
            this.infoBox.Name = "infoBox";
            this.infoBox.Size = new System.Drawing.Size(611, 318);
            this.infoBox.TabIndex = 9;
            this.infoBox.Text = "This method takes two ints as arguments, and returns the highest of the two.";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 24);
            this.label1.TabIndex = 8;
            this.label1.Text = "Author:";
            // 
            // authorBox
            // 
            this.authorBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.authorBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.authorBox.Location = new System.Drawing.Point(307, 12);
            this.authorBox.Name = "authorBox";
            this.authorBox.Size = new System.Drawing.Size(307, 24);
            this.authorBox.TabIndex = 7;
            this.authorBox.Text = "David Leotta";
            this.authorBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(199, 24);
            this.label2.TabIndex = 13;
            this.label2.Text = "Programming Language:";
            // 
            // langBox
            // 
            this.langBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.langBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.langBox.Location = new System.Drawing.Point(311, 60);
            this.langBox.Name = "langBox";
            this.langBox.Size = new System.Drawing.Size(303, 24);
            this.langBox.TabIndex = 14;
            this.langBox.Text = "C#";
            this.langBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InfoWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.langBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.versionBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.infoBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.authorBox);
            this.Name = "InfoWindow";
            this.Size = new System.Drawing.Size(617, 426);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label versionBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label infoBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label authorBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label langBox;
    }
}
