﻿namespace CodeBucket
{
    partial class PreviewBucket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewBucket));
            this.label1 = new System.Windows.Forms.Label();
            this.codePanel1 = new CodeBucket.CodePanel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(465, 104);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // codePanel1
            // 
            this.codePanel1.BackColor = System.Drawing.SystemColors.Control;
            this.codePanel1.Location = new System.Drawing.Point(12, 116);
            this.codePanel1.Name = "codePanel1";
            this.codePanel1.Size = new System.Drawing.Size(465, 183);
            this.codePanel1.TabIndex = 1;
            // 
            // PreviewBucket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(489, 562);
            this.Controls.Add(this.codePanel1);
            this.Controls.Add(this.label1);
            this.Name = "PreviewBucket";
            this.Text = "PreviewBucket";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private CodePanel codePanel1;
    }
}