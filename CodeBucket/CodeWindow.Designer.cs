﻿namespace CodeBucket
{
    partial class CodeWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.codePanel1 = new CodeBucket.CodePanel();
            this.paramBox = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // codePanel1
            // 
            this.codePanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.codePanel1.Location = new System.Drawing.Point(26, 19);
            this.codePanel1.Name = "codePanel1";
            this.codePanel1.Size = new System.Drawing.Size(565, 183);
            this.codePanel1.TabIndex = 6;
            // 
            // paramBox
            // 
            this.paramBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.paramBox.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.paramBox.Location = new System.Drawing.Point(26, 220);
            this.paramBox.Name = "paramBox";
            this.paramBox.Size = new System.Drawing.Size(565, 206);
            this.paramBox.TabIndex = 7;
            this.paramBox.Text = "@param a (int) First number\r\n@param b (int) Second number\r\n\r\n@overload +0\r\n\r\n@ret" +
    "urn (int) The highest number of two inputs.";
            // 
            // CodeWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.paramBox);
            this.Controls.Add(this.codePanel1);
            this.Name = "CodeWindow";
            this.Size = new System.Drawing.Size(617, 426);
            this.ResumeLayout(false);

        }

        #endregion
        private CodePanel codePanel1;
        private System.Windows.Forms.Label paramBox;
    }
}
