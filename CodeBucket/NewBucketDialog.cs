﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class NewBucketDialog : Form
    {

        Form1 form1;

        bool formMove = false;
        int mouseDownX;
        int mouseDownY;

        MessagePopup popupName;
        MessagePopup popupLang;

        public const int WM_CREATE = 0x1;

        [DllImport("uxtheme.dll", CharSet = CharSet.Unicode)]
        public extern static int SetWindowTheme(
            IntPtr hWnd, string pszSubAppName, string pszSubIdList);

        public NewBucketDialog(Form1 form)
        {
            InitializeComponent();
            SetWindowTheme(comboBox1.Handle, "explorer", null);
            form1 = form;

            popupName = new MessagePopup();
            popupName.Visible = true;
            popupName.Visible = false;

            popupLang = new MessagePopup();
            popupLang.Visible = true;
            popupLang.Visible = false;

            AddHandlers();
            AddMovementHandlers();
        }

        private void AddHandlers()
        {
            actionButton1.button.Click += new EventHandler(Create_Click);
        }

        private void AddMovementHandlers()
        {
            //Panel2.MouseDown += new MouseEventHandler(Window_MouseDown);
            //licenseName.MouseDown += new MouseEventHandler(Window_MouseDown);

            //Panel2.MouseMove += new MouseEventHandler(Window_MouseMove);
            //licenseName.MouseDown += new MouseEventHandler(Window_MouseMove);

            //Panel2.MouseUp += new MouseEventHandler(Window_MouseUp);
            //licenseName.MouseUp += new MouseEventHandler(Window_MouseUp);
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NewBucketDialog_Shown(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (form1.NameExists(textBox1.Text))
            {
                int xLoc = (textBox1.Location.X + textBox1.Width);
                int yLoc = textBox1.Location.Y + (textBox1.Height / 2);

                Point point = textBox1.PointToScreen(Point.Empty);
                point.X += (textBox1.Width - 10);
                point.Y -= (textBox1.Height);


                popupName.SetLocation(point);
                popupLang.SetLocation(point);

                
                popupName.SetText("A bucket with that name already exists. Please choose another name.");
                
                popupName.Visible = true;
            } else
            {
                try
                {
                    popupName.Visible = false;
                }
                catch { }
                
            }
        }

        private void NewBucketDialog_LocationChanged(object sender, EventArgs e)
        {
            Point point = textBox1.PointToScreen(Point.Empty);
            point.X += (textBox1.Width - 10);
            point.Y -= (textBox1.Height);


            popupName.SetLocation(point);
            popupLang.SetLocation(point);
        }

        private void Window_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                formMove = true;
                mouseDownX = e.X;
                mouseDownY = e.Y;
            }
        }

        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if(formMove)
            {
                Point point = new Point();
                point.X = this.Location.X + (e.X - mouseDownX);
                point.Y = this.Location.Y + (e.Y - mouseDownY);
                this.Location = point;
            }
        }

        private void Window_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                formMove = false;
        }

        private void NewBucketDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            popupName.Close();
            popupLang.Close();
        }

        private void Close_MouseEnter(object sender, EventArgs e)
        {
            this.ForeColor = Color.Black;
        }

        private void Close_MouseLeave(object sender, EventArgs e)
        {
            this.ForeColor = Color.FromKnownColor(KnownColor.ControlDark);
        }

        private void Create_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                Point point = textBox1.PointToScreen(Point.Empty);
                point.X += (textBox1.Width - 10);
                point.Y -= (textBox1.Height);


                popupName.SetLocation(point);
                popupName.Visible = true;
            }
            if (comboBox1.Text == "")
            {
                Point point = comboBox1.PointToScreen(Point.Empty);
                point.X += (comboBox1.Width - 10);
                point.Y -= (comboBox1.Height);

                popupLang.SetLocation(point);
                popupLang.SetText("You need to choose a programming language.");

                popupLang.Visible = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           popupLang.Visible = false;
        }
    }
}