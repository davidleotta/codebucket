﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class CodeButton : UserControl
    {

        bool focus;
        Form1 form1;

        public CodeButton(Form1 form)
        {
            InitializeComponent();
            form1 = form;
        }

        public void SetFocus(bool val)
        {
            focus = val;

            if (val)
            {
                label1.BackColor = Color.FromKnownColor(KnownColor.WhiteSmoke);
                bottomPanel.Visible = false;
            }
            else
            {
                label1.BackColor = Color.FromKnownColor(KnownColor.Control);
                bottomPanel.Visible = true;
            }
        }

        private void CodeButton_MouseEnter(object sender, EventArgs e)
        {
            if (focus)
            {
                label1.BackColor = Color.FromKnownColor(KnownColor.WhiteSmoke);
            }
            else
            {
                label1.BackColor = Color.FromKnownColor(KnownColor.WhiteSmoke);
            }
        }

        private void CodeButton_MouseLeave(object sender, EventArgs e)
        {
            if (focus)
            {
                label1.BackColor = Color.FromKnownColor(KnownColor.WhiteSmoke);
            }
            else
            {
                label1.BackColor = Color.FromKnownColor(KnownColor.Control);
            }
        }

        private void CodeButton_Click(object sender, EventArgs e)
        {
            form1.SetButtonFocus(1);
        }

        public void ExpandBorder()
        {
            expandTimer.Enabled = true;
            contractTimer.Enabled = false;
        }

        public void ContractBorder()
        {
            expandTimer.Enabled = false;
            contractTimer.Enabled = true;
        }

        private void contractTimer_Tick(object sender, EventArgs e)
        {
            while (bottomPanel.Width != 0)
            {
                bottomPanel.Location = new Point(bottomPanel.Location.X + 2, bottomPanel.Location.Y);
                bottomPanel.Size = new Size(bottomPanel.Width - 2, bottomPanel.Height);
            }
            contractTimer.Enabled = false;
        }

        private void expandTimer_Tick(object sender, EventArgs e)
        {
            while (bottomPanel.Width != 76)
            {
                bottomPanel.Location = new Point(bottomPanel.Location.X - 2, bottomPanel.Location.Y);
                bottomPanel.Size = new Size(bottomPanel.Width + 2, bottomPanel.Height);
            }
            expandTimer.Enabled = false;
        }
    }

}