﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class ImportBucket : Form
    {

        Form1 form1;

        public ImportBucket(Form1 form)
        {
            InitializeComponent();

            form1 = form;
        }

        private void ImportBucket_Shown(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
        }
    }
}
