﻿namespace CodeBucket
{
    partial class CodeEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel7 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.licenseName = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.Panel2.SuspendLayout();
            this.Panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(651, 1);
            this.Panel7.TabIndex = 1;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.Panel2.Controls.Add(this.Label6);
            this.Panel2.Controls.Add(this.label1);
            this.Panel2.Controls.Add(this.licenseName);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(651, 36);
            this.Panel2.TabIndex = 146;
            // 
            // Label6
            // 
            this.Label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label6.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label6.Location = new System.Drawing.Point(610, 5);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 27);
            this.Label6.TabIndex = 3;
            this.Label6.Text = "X";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(500, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 27);
            this.label1.TabIndex = 143;
            this.label1.Text = "Code Editor";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // licenseName
            // 
            this.licenseName.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.licenseName.ForeColor = System.Drawing.Color.Black;
            this.licenseName.Location = new System.Drawing.Point(11, 5);
            this.licenseName.Name = "licenseName";
            this.licenseName.Size = new System.Drawing.Size(483, 27);
            this.licenseName.TabIndex = 1;
            this.licenseName.Text = "Find Highest";
            this.licenseName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(651, 1);
            this.panel23.TabIndex = 2;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(651, 1);
            this.Panel3.TabIndex = 145;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.SystemColors.Control;
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 396);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(651, 27);
            this.Panel9.TabIndex = 149;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 423);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(651, 1);
            this.Panel10.TabIndex = 150;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(652, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 424);
            this.Panel8.TabIndex = 148;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 424);
            this.Panel4.TabIndex = 144;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel6.Location = new System.Drawing.Point(0, 424);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(653, 1);
            this.Panel6.TabIndex = 147;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Font = new System.Drawing.Font("Consolas", 10F);
            this.richTextBox1.Location = new System.Drawing.Point(12, 43);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(629, 347);
            this.richTextBox1.TabIndex = 151;
            this.richTextBox1.Text = "my code";
            // 
            // CodeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(653, 425);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel6);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CodeEditor";
            this.Text = "CodeEditor";
            this.Panel2.ResumeLayout(false);
            this.Panel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel Panel7;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label licenseName;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Panel Panel6;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}