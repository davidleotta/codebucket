﻿namespace CodeBucket
{
    partial class ImportBucket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.CloseButton = new System.Windows.Forms.Label();
            this.licenseName = new System.Windows.Forms.Label();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Panel7 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.listButton2 = new CodeBucket.ListButton();
            this.listButton1 = new CodeBucket.ListButton();
            this.Panel9.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(524, 27);
            this.label2.TabIndex = 191;
            this.label2.Text = "_";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(546, 1);
            this.Panel3.TabIndex = 184;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.SystemColors.Control;
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 255);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(546, 27);
            this.Panel9.TabIndex = 188;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(546, 1);
            this.panel23.TabIndex = 2;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 282);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(546, 1);
            this.Panel10.TabIndex = 189;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(547, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 283);
            this.Panel8.TabIndex = 187;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 283);
            this.Panel4.TabIndex = 183;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel6.Location = new System.Drawing.Point(0, 283);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(548, 1);
            this.Panel6.TabIndex = 186;
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.CloseButton.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.CloseButton.Location = new System.Drawing.Point(505, 5);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(38, 27);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "X";
            this.CloseButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // licenseName
            // 
            this.licenseName.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.licenseName.ForeColor = System.Drawing.Color.Black;
            this.licenseName.Location = new System.Drawing.Point(11, 5);
            this.licenseName.Name = "licenseName";
            this.licenseName.Size = new System.Drawing.Size(488, 27);
            this.licenseName.TabIndex = 1;
            this.licenseName.Text = "Import Bucket";
            this.licenseName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.Panel2.Controls.Add(this.CloseButton);
            this.Panel2.Controls.Add(this.licenseName);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(546, 36);
            this.Panel2.TabIndex = 185;
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(546, 1);
            this.Panel7.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(524, 27);
            this.label1.TabIndex = 190;
            this.label1.Text = "Browse for a Bucket";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // listButton2
            // 
            this.listButton2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton2.Location = new System.Drawing.Point(181, 150);
            this.listButton2.Name = "listButton2";
            this.listButton2.Size = new System.Drawing.Size(186, 28);
            this.listButton2.TabIndex = 198;
            this.listButton2.Text = "Browse";
            this.listButton2.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listButton1
            // 
            this.listButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton1.Location = new System.Drawing.Point(425, 221);
            this.listButton1.Name = "listButton1";
            this.listButton1.Size = new System.Drawing.Size(111, 28);
            this.listButton1.TabIndex = 199;
            this.listButton1.Text = "Import";
            this.listButton1.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ImportBucket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 284);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.listButton1);
            this.Controls.Add(this.listButton2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel6);
            this.Controls.Add(this.label1);
            this.Name = "ImportBucket";
            this.Text = "ImportBucket";
            this.Shown += new System.EventHandler(this.ImportBucket_Shown);
            this.Panel9.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.Label CloseButton;
        internal System.Windows.Forms.Label licenseName;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Panel Panel7;
        internal System.Windows.Forms.Label label1;
        private ListButton listButton2;
        private ListButton listButton1;
    }
}