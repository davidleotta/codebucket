﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBucket
{
    class BucketPointer
    {
        string name;
        string fileLoc;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str">Name of the bucket (file name without extension)</param>
        /// <param name="loc">File location of the bucket</param>
        public BucketPointer(string str, string loc)
        {
            name = str;
            fileLoc = loc;
        }

        public string GetName() { return name; }

        public string GetLocation() { return fileLoc; }
    }
}
