﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class Form1 : Form
    {

        InfoButton infoButton;
        CodeButton codeButton;

        NewBucketDialog newBucketDialog;

        MessagePopup messagepopup;

        public MenuView menu;

        Settings settings;

        Boolean isMenuVisible = true;

        ArrayList bucketNames;

        InfoWindow infoWindow;
        CodeWindow codeWindow;

        int buttonY = 3;
        int buttonCount = 0;


        //ArrayList to store all buckets
        ArrayList buckets;

        //ArrayList to store list buttons
        ArrayList bucketButtons;

        public Form1()
        {
            InitializeComponent();

            AddBucketControls();

            buckets = new ArrayList();

            bucketNames = new ArrayList { "codebucket", "Find Highest", "test bucket" };
            //populate bucket names here
            //but for now, use example names

            messagepopup = new MessagePopup();
            messagepopup.Visible = true;
            messagepopup.Visible = false;

            menu = new MenuView(this);
            this.Controls.Add(menu);
            menu.Location = new Point(0, 37);//66
            menu.Visible = true;
            menu.Visible = false;
            menu.BringToFront();

            SetupWindows();

            Bucket newBucket = new Bucket(@"G:\Find Highest.bucket");
            codeWindow.LoadBucket(newBucket);
            infoWindow.LoadBucket(newBucket);
            label1.Text = newBucket.title;

            
            settings = new Settings(this);

            AddHoverHandlers();

            LoadBucketList();

        }

        private void SetupWindows()
        {
            infoWindow = new InfoWindow();
            codeWindow = new CodeWindow();

            splitContainer1.Panel2.Controls.Add(infoWindow);
            infoWindow.Dock = DockStyle.Fill;
            infoWindow.BringToFront();

            splitContainer1.Panel2.Controls.Add(codeWindow);
            codeWindow.Dock = DockStyle.Fill;
            codeWindow.BringToFront();
            codeWindow.Visible = false; //start as invisible
        }

        private void AddBucketControls()
        {
            infoButton = new InfoButton(this);
            codeButton = new CodeButton(this);

            panel5.Controls.Add(infoButton);
            infoButton.Location = new Point(418, 9);//317
            infoButton.Anchor = (AnchorStyles.Right | AnchorStyles.Top);

            panel5.Controls.Add(codeButton);
            codeButton.Location = new Point(499, 9);//399
            codeButton.Anchor = (AnchorStyles.Right | AnchorStyles.Top);
            codeButton.SetFocus(false);

            infoButton.BringToFront();
            codeButton.BringToFront();


        }

        private void AddHoverHandlers()
        {
            label3.MouseHover += new EventHandler(PopupHover);
            label3.MouseLeave += new EventHandler(PopupLeave);
        }

        private void PopupHover(object sender, EventArgs e)
        {
            Control uc = (Control)sender;

            int xLoc = (uc.Location.X + uc.Width);
            int yLoc = uc.Location.Y + (uc.Height / 2);

            Point point = uc.PointToScreen(Point.Empty);
            point.X += uc.Width - 10;
            point.Y -= uc.Height;

            messagepopup.SetLocation(point);

            messagepopup.Visible = true;

        }

        private void PopupLeave(object sender, EventArgs e)
        {
            messagepopup.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            try
            {
                if (newBucketDialog == null)
                    newBucketDialog = new NewBucketDialog(this);
                newBucketDialog.Show();
            } catch (ObjectDisposedException)
            {
                newBucketDialog = new NewBucketDialog(this);
                newBucketDialog.Show();
            }
        }

        private void label3_MouseHover(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (menu.Visible)
            {
                menu.Visible = false;
                button2.BackColor = Color.Transparent;
            }
            else
            {
                menu.Visible = true;
                button2.BackColor = Color.FromArgb(229, 229, 229);
                //this.Refresh()
                menu.Refresh();
            }
        }

        private void RepaintMenuHeader(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Point[] slantArray = new Point[5];
            slantArray[0] = new Point(96, 38);
            slantArray[1] = new Point(106 + 127, 38);
            slantArray[2] = new Point(106 + 127, 66);
            slantArray[3] = new Point(106, 66);
            slantArray[4] = new Point(92, 66);

            g.FillPolygon(Brushes.AntiqueWhite, slantArray);

            e.Graphics.FillRectangle(Brushes.Black, new Rectangle(new Point(100, 100), new Size(200, 200)));

        }

        public void OpenSettings()
        {
            settings.Show();
        }

        /// <summary>
        /// Toggles menu panel (left split panel) visibility
        /// Relies on (bool)isMenuVisible
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (isMenuVisible)
            {
                splitContainer1.Panel1Collapsed = true;
                //Animations are currently broken
                //contractSidePanel.Enabled = true;
                //expandSidePanel.Enabled = false;
                isMenuVisible = false;
            } else
            {
                splitContainer1.Panel1Collapsed = false;
                //Animations are currently broken
                //expandSidePanel.Enabled = true;
                //contractSidePanel.Enabled = false;
                isMenuVisible = true;
            }
        }

        public void SetButtonFocus(int i)
        {
            if (i == 0)
            {
                //InfoButton
                infoButton.SetFocus(true);
                codeButton.SetFocus(false);
                //infoButton.ContractBorder();
                //codeButton.ExpandBorder();

                infoWindow.Visible = true;
                codeWindow.Visible = false;
            } else
            {
                //CodeButton
                infoButton.SetFocus(false);
                codeButton.SetFocus(true);
                //infoButton.ExpandBorder();
                //codeButton.ContractBorder();

                infoWindow.Visible = false;
                codeWindow.Visible = true;
            }
        }

        private void contractSidePanel_Tick(object sender, EventArgs e)
        {
            if (splitContainer1.SplitterDistance != 0)
                splitContainer1.SplitterDistance -= 23;
            else
                contractSidePanel.Enabled = false;
        }

        private void expandSidePanel_Tick(object sender, EventArgs e)
        {
            if (splitContainer1.SplitterDistance != 230)
                splitContainer1.SplitterDistance += 23;
            else
                contractSidePanel.Enabled = false;
        }



        public void ShowConsole()
        {

        }

        public void WriteConsole(string str)
        {

        }

        public Boolean NameExists(string str)
        {
            //TODO ignore case
            if (bucketNames.Contains(str))
                return true;
            return false;
        }

        public void LoadBucketList()
        {
            //Load list from file
            //ProgramDirectory/Buckets.codebucket

            buckets.Add(new BucketPointer("Find Highest", @"G:\Find Highest.bucket"));
            AddNewButton();

            buckets.Add(new BucketPointer("Bucket", @"G:\CodeBucket\ExampleBuckets\Bucket.bucket"));
            AddNewButton();

            buckets.Add(new BucketPointer("mybucket", @"G:\CodeBucket\ExampleBuckets\mybucket"));
            AddNewButton();
        }

        public void AddNewButton()
        {
            ListButton lb = new ListButton();
            splitContainer1.Panel1.Controls.Add(lb);
            lb.Text = "New Button";
            lb.Location = new Point(0, buttonY);
            lb.button4.Tag = buttonCount;
            lb.button4.Click += new EventHandler(ListButton_Click);

            buttonCount += 1;
            buttonY += 28;
        }

        public void ListButton_Click(object sender, EventArgs e)
        {
            Button lb = (Button)sender;

            int i = (int)lb.Tag;

            LoadBucket((int)lb.Tag);
        }

        public void LoadBucket(int index)
        {
            //get folder location (ArrayList) bucket[index]
        }
    }
}