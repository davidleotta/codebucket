﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class MenuView : UserControl
    {

        Pen tanPen = new Pen(Brushes.Tan);
        Pen transparentPen = new Pen(Brushes.Transparent);

        Form1 form1;


        NewBucketDialog newBucketDialog;
        Print print;
        ImportBucket import;
        //Export
        SendToDistributionLab distLab;
        //console
        Settings settings;
        //about


        public MenuView(Form1 form)
        {
            InitializeComponent();

            form1 = form;

            AddButtonHandlers();

        }

        private void AddButtonHandlers()
        {
            newButton.ButtonClick += new EventHandler(New_ButtonClick);
            printButton.ButtonClick += new EventHandler(Print_ButtonClick);
            importButton.ButtonClick += new EventHandler(Import_ButtonClick);
            //Export
            sendButton.ButtonClick += new EventHandler(Send_ButtonClick);
            consoleButton.ButtonClick += new EventHandler(console_ButtonClick);
            settingsButton.ButtonClick += new EventHandler(Settings_ButtonClick);
            //about
        }

        private void MenuView_Paint(object sender, PaintEventArgs e)
        {
            //Graphics g = e.Graphics;

           // Point[] slantArray = new Point[5];
            //slantArray[0] = new Point(96, 0);//106
            //slantArray[1] = new Point(106 + 127, 0);
            //slantArray[2] = new Point(106 + 127, 28);
            //slantArray[3] = new Point(106, 28);
            //slantArray[4] = new Point(92, 28);//102

            //g.FillPolygon(Brushes.AntiqueWhite, slantArray);

            //Point[] boxArray = new Point[3];
            //g.FillRectangle(Brushes.AntiqueWhite, boxArray);

            //g.DrawLine(tanPen, new Point(92, 28), new Point(96, 0));
        }

        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void New_ButtonClick(object sender, EventArgs e)
        {
            if (newBucketDialog == null)
                newBucketDialog = new NewBucketDialog(form1);

            newBucketDialog.Show();
            form1.menu.Visible = false;
        }

        private void Print_ButtonClick(object sender, EventArgs e)
        {
            if (print == null)
                print = new Print(form1);

            print.Show();
            form1.menu.Visible = false;
        }

        private void console_ButtonClick(object sender, EventArgs e)
        {
            
        }

        private void Send_ButtonClick(object sender, EventArgs e)
        {
            if (distLab == null)
                distLab = new SendToDistributionLab();
            distLab.Show();
            form1.menu.Visible = false;
        }

        private void Settings_ButtonClick(object sender, EventArgs e)
        {
            if (settings == null)
                settings = new Settings(form1);

            settings.Show();
            form1.menu.Visible = false;
        }

        private void Import_ButtonClick(object sender, EventArgs e)
        {
            if (import == null)
                import = new ImportBucket(form1);

            import.Show();
            form1.menu.Visible = false;
        }
    }
}