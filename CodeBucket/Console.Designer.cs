﻿namespace CodeBucket
{
    partial class Console
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CloseButton = new System.Windows.Forms.Label();
            this.consoleName = new System.Windows.Forms.Label();
            this.Panel7 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Panel2.SuspendLayout();
            this.Panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.CloseButton.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.CloseButton.Location = new System.Drawing.Point(528, 5);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(38, 27);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "X";
            this.CloseButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // consoleName
            // 
            this.consoleName.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.consoleName.ForeColor = System.Drawing.Color.White;
            this.consoleName.Location = new System.Drawing.Point(11, 5);
            this.consoleName.Name = "consoleName";
            this.consoleName.Size = new System.Drawing.Size(488, 27);
            this.consoleName.TabIndex = 1;
            this.consoleName.Text = "Terminal";
            this.consoleName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(569, 1);
            this.Panel7.TabIndex = 1;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(569, 1);
            this.panel23.TabIndex = 2;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(33)))), ((int)(((byte)(32)))));
            this.Panel2.Controls.Add(this.CloseButton);
            this.Panel2.Controls.Add(this.consoleName);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(569, 36);
            this.Panel2.TabIndex = 185;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.DimGray;
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(569, 1);
            this.Panel3.TabIndex = 184;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(33)))), ((int)(((byte)(32)))));
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 269);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(569, 27);
            this.Panel9.TabIndex = 188;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.DimGray;
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 296);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(569, 1);
            this.Panel10.TabIndex = 189;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.DimGray;
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(570, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 297);
            this.Panel8.TabIndex = 187;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.DimGray;
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 297);
            this.Panel4.TabIndex = 183;
            // 
            // listBox1
            // 
            this.listBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(23)))), ((int)(((byte)(22)))));
            this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.Font = new System.Drawing.Font("Consolas", 10F);
            this.listBox1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 15;
            this.listBox1.Items.AddRange(new object[] {
            "CodeBucket v1.0",
            "Copyright (C) David Leotta 2011-2017"});
            this.listBox1.Location = new System.Drawing.Point(1, 37);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(569, 232);
            this.listBox1.TabIndex = 190;
            // 
            // Console
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(23)))), ((int)(((byte)(22)))));
            this.ClientSize = new System.Drawing.Size(571, 297);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Console";
            this.Text = "Console";
            this.Panel2.ResumeLayout(false);
            this.Panel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label CloseButton;
        internal System.Windows.Forms.Label consoleName;
        internal System.Windows.Forms.Panel Panel7;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel4;
        private System.Windows.Forms.ListBox listBox1;
    }
}