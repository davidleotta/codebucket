﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBucket
{
    class SyntaxHighlighting
    {
        public static ArrayList keywords = new ArrayList
        {
            "abstract", "as", "base", "bool", "break",
            "case", "catch", "checked", "class", "const",
            "continue", "default", "delegate", "do", "else",
            "enum", "event", "explicit", "extern", "finally",
            "fixed", "for", "foreach", "goto", "if", "implicit",
            "in", "interface", "internal", "is", "lock", "namespace",
            "new", "null", "object", "operator", "out", "override",
            "params", "private", "protected", "public", "readonly",
            "ref", "return", "sealed", "sizeof", "stackalloc", "static",
            "struct", "switch", "this", "throw", "try", "typeof", "unchecked",
            "unsafe", "using", "virtual", "volatile", "void", "while"
        };

        public static ArrayList datatypes = new ArrayList
        {
            "byte", "char", "decimal", "double", "float",
            "int", "long", "sbyte", "short", "string", "uint",
            "ulong", "ushort"
        };

        public static ArrayList bools = new ArrayList
        { "true", "false" };

        public static Boolean CSharp(string str)
        {
            if (str == "private" || str == "public")
                return true;

            return false;
        }
    }
}
