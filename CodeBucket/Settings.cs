﻿using CodeBucket.Settings_Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class Settings : Form
    {

        //General
        //General? -- change
        //Buckets
        Settings_Design s_d;

        //Backup
        //Local Backup
        //Cloud Provider

        //Software
        //Check for update

        //Regulatory
        Settings_OpenSource s_os;

        Settings_SyntaxHighlighting s_sh;


        Form1 form1;



        public Settings(Form1 form)
        {
            InitializeComponent();

            form1 = form;



            //Define new settings forms
            s_d = new Settings_Design();
            s_sh = new Settings_SyntaxHighlighting();
            s_os = new Settings_OpenSource();

        }

        private void CloseAllForms()
        {
            try
            {
                this.Controls.Remove(s_os);
            } catch (Exception e)
            {
                //Supress errors
                //Maybe print to console
            }
            
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (!IsVisible(s_os))
                OpenForm(s_os);
        }

        private void OpenForm(UserControl form)
        {
            CloseAllForms();

            //Shouldnt need to cast
            this.Controls.Add(form);
            form.Dock = DockStyle.Fill;
            form.BringToFront();
        }

        private Boolean IsVisible(UserControl form)
        {
            if (this.Controls.Contains(form))
                return true;

            return false;


        }

        private const int CS_DROPSHADOW = 0x00020000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void Settings_Shown(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.None;
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (!IsVisible(s_sh))
                OpenForm(s_sh);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (!IsVisible(s_d))
                OpenForm(s_d);
        }
    }
}
