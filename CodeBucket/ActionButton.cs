﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class ActionButton : UserControl
    {
        //112, 192, 231


        public ActionButton()
        {
            InitializeComponent();
        }

        [Browsable(true), Category("Label"), Description("Label on the button"),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Bindable(true)]
        public override string Text
        {
            get
            {
                return button.Text;
            }
            set
            {
                button.Text = value;
            }
        }

        [Browsable(true), Category("Label"), Description("Label on the button"),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Bindable(true)]
        public Color BorderColor
        {
            get
            {
                return topPanel.BackColor;
            }
            set
            {
                topPanel.BackColor = value;
                bottomPanel.BackColor = value;
                leftPanel.BackColor = value;
                rightPanel.BackColor = value;
            }
        }

        [Browsable(true), Category("Label"), Description("Label on the button"),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Bindable(true)]
        public override Color BackColor
        {
            get
            {
                return button.BackColor;
            }
            set
            {
                button.BackColor = value;
            }
        }




    }
}
