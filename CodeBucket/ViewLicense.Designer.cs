﻿namespace CodeBucket.Settings_Forms
{
    partial class ViewLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.licenseName = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Panel7 = new System.Windows.Forms.Panel();
            this.licensePicture = new System.Windows.Forms.PictureBox();
            this.licenseBox = new System.Windows.Forms.RichTextBox();
            this.Panel9.SuspendLayout();
            this.Panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.licensePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(12, 435);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(621, 27);
            this.label1.TabIndex = 4;
            this.label1.Text = "<License Name> Version <License Version>";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel6.Location = new System.Drawing.Point(0, 493);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(645, 1);
            this.Panel6.TabIndex = 136;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 493);
            this.Panel4.TabIndex = 133;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(644, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 493);
            this.Panel8.TabIndex = 137;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 492);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(643, 1);
            this.Panel10.TabIndex = 140;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.SystemColors.Control;
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 465);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(643, 27);
            this.Panel9.TabIndex = 139;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(643, 1);
            this.Panel3.TabIndex = 134;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(643, 1);
            this.panel23.TabIndex = 2;
            // 
            // licenseName
            // 
            this.licenseName.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.licenseName.ForeColor = System.Drawing.Color.Black;
            this.licenseName.Location = new System.Drawing.Point(11, 5);
            this.licenseName.Name = "licenseName";
            this.licenseName.Size = new System.Drawing.Size(585, 27);
            this.licenseName.TabIndex = 1;
            this.licenseName.Text = "<License Name>";
            this.licenseName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label6.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label6.Location = new System.Drawing.Point(602, 5);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 27);
            this.Label6.TabIndex = 3;
            this.Label6.Text = "X";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.Panel2.Controls.Add(this.Label6);
            this.Panel2.Controls.Add(this.licenseName);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(643, 36);
            this.Panel2.TabIndex = 135;
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(643, 1);
            this.Panel7.TabIndex = 1;
            // 
            // licensePicture
            // 
            this.licensePicture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.licensePicture.Location = new System.Drawing.Point(12, 43);
            this.licensePicture.Name = "licensePicture";
            this.licensePicture.Size = new System.Drawing.Size(621, 78);
            this.licensePicture.TabIndex = 141;
            this.licensePicture.TabStop = false;
            // 
            // licenseBox
            // 
            this.licenseBox.BackColor = System.Drawing.SystemColors.Control;
            this.licenseBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.licenseBox.Location = new System.Drawing.Point(12, 127);
            this.licenseBox.Name = "licenseBox";
            this.licenseBox.Size = new System.Drawing.Size(621, 305);
            this.licenseBox.TabIndex = 142;
            this.licenseBox.Text = "";
            // 
            // ViewLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(645, 494);
            this.Controls.Add(this.licenseBox);
            this.Controls.Add(this.licensePicture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ViewLicense";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ViewLicense";
            this.Load += new System.EventHandler(this.ViewLicense_Load);
            this.Panel9.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.licensePicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Label licenseName;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Panel Panel7;
        private System.Windows.Forms.PictureBox licensePicture;
        private System.Windows.Forms.RichTextBox licenseBox;
    }
}