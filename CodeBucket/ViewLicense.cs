﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket.Settings_Forms
{
    public partial class ViewLicense : Form
    {

        string software;

        public ViewLicense()
        {
            InitializeComponent();
        }

        private void ViewLicense_Load(object sender, EventArgs e)
        {
            switch (software)
            {
                case "HTML-Renderer":
                    LoadHTMLRenderer();
                    break;
                case "Icons8":
                    LoadIcons8();
                    break;
            }

            MessageBox.Show(software);

        }

        public void LoadLicense(string str)
        {
            software = str;
            licenseName.Text = software;
        }

        private void LoadHTMLRenderer()
        {

        }

        private void LoadIcons8()
        {
            licenseBox.Text = Properties.Resources.Icons8License.ToString();
        }

    }
}
