﻿namespace CodeBucket
{
    partial class NewBucketDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel7 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.CloseButton = new System.Windows.Forms.Label();
            this.licenseName = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.actionButton1 = new CodeBucket.ActionButton();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Panel2.SuspendLayout();
            this.Panel9.SuspendLayout();
            this.panel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(546, 1);
            this.Panel7.TabIndex = 1;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.Panel2.Controls.Add(this.CloseButton);
            this.Panel2.Controls.Add(this.licenseName);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(546, 36);
            this.Panel2.TabIndex = 146;
            this.Panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Window_MouseDown);
            this.Panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Window_MouseMove);
            this.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Window_MouseUp);
            // 
            // CloseButton
            // 
            this.CloseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseButton.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.CloseButton.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.CloseButton.Location = new System.Drawing.Point(505, 5);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(38, 27);
            this.CloseButton.TabIndex = 3;
            this.CloseButton.Text = "X";
            this.CloseButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            this.CloseButton.MouseEnter += new System.EventHandler(this.Close_MouseEnter);
            this.CloseButton.MouseLeave += new System.EventHandler(this.Close_MouseLeave);
            // 
            // licenseName
            // 
            this.licenseName.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.licenseName.ForeColor = System.Drawing.Color.Black;
            this.licenseName.Location = new System.Drawing.Point(11, 5);
            this.licenseName.Name = "licenseName";
            this.licenseName.Size = new System.Drawing.Size(488, 27);
            this.licenseName.TabIndex = 1;
            this.licenseName.Text = "New CodeBucket";
            this.licenseName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.licenseName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Window_MouseDown);
            this.licenseName.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Window_MouseMove);
            this.licenseName.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Window_MouseUp);
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(546, 1);
            this.panel23.TabIndex = 2;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(546, 1);
            this.Panel3.TabIndex = 145;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.SystemColors.Control;
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 450);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(546, 27);
            this.Panel9.TabIndex = 149;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 477);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(546, 1);
            this.Panel10.TabIndex = 150;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(547, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 478);
            this.Panel8.TabIndex = 148;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 478);
            this.Panel4.TabIndex = 144;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel6.Location = new System.Drawing.Point(0, 478);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(548, 1);
            this.Panel6.TabIndex = 147;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(12, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(524, 27);
            this.label1.TabIndex = 151;
            this.label1.Text = "Bucket Title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(20, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(524, 27);
            this.label2.TabIndex = 152;
            this.label2.Text = "(This can be changed later)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(524, 27);
            this.label3.TabIndex = 154;
            this.label3.Text = "Programming Language";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "C",
            "C++",
            "C#",
            "CSS",
            "HTML",
            "Java",
            "SQL"});
            this.comboBox1.Location = new System.Drawing.Point(113, 196);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(322, 25);
            this.comboBox1.TabIndex = 156;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(524, 27);
            this.label4.TabIndex = 157;
            this.label4.Text = "(This can also be changed later)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Visible = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.AntiqueWhite;
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Controls.Add(this.panel15);
            this.panel11.Controls.Add(this.linkLabel1);
            this.panel11.Controls.Add(this.pictureBox2);
            this.panel11.Controls.Add(this.label5);
            this.panel11.Location = new System.Drawing.Point(12, 329);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(524, 34);
            this.panel11.TabIndex = 182;
            this.panel11.Visible = false;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Tan;
            this.panel12.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel12.Location = new System.Drawing.Point(523, 1);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1, 32);
            this.panel12.TabIndex = 4;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Tan;
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 1);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(1, 32);
            this.panel13.TabIndex = 3;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Tan;
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(0, 33);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(524, 1);
            this.panel14.TabIndex = 2;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Tan;
            this.panel15.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(524, 1);
            this.panel15.TabIndex = 1;
            // 
            // linkLabel1
            // 
            this.linkLabel1.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.linkLabel1.LinkColor = System.Drawing.Color.Red;
            this.linkLabel1.Location = new System.Drawing.Point(450, 3);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(71, 28);
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Dismiss";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::CodeBucket.Properties.Resources.Icon_Error;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(3, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(28, 28);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label5.Location = new System.Drawing.Point(37, 7);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(407, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Select a programming language to continue.";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.textBox1.Location = new System.Drawing.Point(52, 85);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(448, 25);
            this.textBox1.TabIndex = 153;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // actionButton1
            // 
            this.actionButton1.BorderColor = System.Drawing.Color.Silver;
            this.actionButton1.Location = new System.Drawing.Point(419, 410);
            this.actionButton1.Name = "actionButton1";
            this.actionButton1.Size = new System.Drawing.Size(117, 34);
            this.actionButton1.TabIndex = 183;
            this.actionButton1.Text = "Create";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic);
            this.label6.ForeColor = System.Drawing.Color.Silver;
            this.label6.Location = new System.Drawing.Point(12, 224);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(524, 27);
            this.label6.TabIndex = 185;
            this.label6.Text = "This can also be changed later";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Italic);
            this.label7.ForeColor = System.Drawing.Color.Silver;
            this.label7.Location = new System.Drawing.Point(12, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(524, 27);
            this.label7.TabIndex = 184;
            this.label7.Text = "This can be changed later";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NewBucketDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(548, 479);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.actionButton1);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "NewBucketDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NewBucketDialog";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewBucketDialog_FormClosing);
            this.Shown += new System.EventHandler(this.NewBucketDialog_Shown);
            this.LocationChanged += new System.EventHandler(this.NewBucketDialog_LocationChanged);
            this.Panel2.ResumeLayout(false);
            this.Panel9.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel7;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Label CloseButton;
        internal System.Windows.Forms.Label licenseName;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private ActionButton actionButton1;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label7;
    }
}