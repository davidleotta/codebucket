﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class CodeEditor : Form
    {

        CodePanel codePanel;

        public CodeEditor(CodePanel cp)
        {
            InitializeComponent();

            codePanel = cp;
        }

        public void SetCode(string code)
        {
            richTextBox1.Text = code;
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //Temporary save button
            UpdateCode();
        }

        private void UpdateCode()
        {
            codePanel.UpdateCode(richTextBox1.Text);
        }
    }
}
