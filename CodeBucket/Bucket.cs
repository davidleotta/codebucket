﻿using CodeBucket.Exceptions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public class Bucket
    {
        public string @fileLoc;
        public string title;




        public string author;
        public string code;
        public string codeinfo;
        public string lang;
        public string param;
        public string version;

        public string bucketversion = "v1"; //assume v1


        ArrayList controlList;

        public Bucket(string file)
        {
            controlList = new ArrayList();
            fileLoc = file;


            if (LoadBucket())
            {

            } else
            {
                throw new IOFailedOpenBucket("Failed to Open Bucket");
            }
            
        }

        public bool VerifyBucket(string file)
        {
            //Debugging
            //throw new NotImplementedException();

            return true;
        }

        public void SaveBucket()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads information from file to the current bucket
        /// </summary>
        /// <returns>true iff successful, false if otherwise</returns>
        public bool LoadBucket()
        {
            //Check file exists
            if (!Directory.Exists(fileLoc))
                throw new IOFileNotFound("The file: " + Path.GetFileName(fileLoc) + " does not exist.");
            //Check file is valid

            FileIOPermission ioPermission = new FileIOPermission(PermissionState.Unrestricted);

            ioPermission.AllLocalFiles = FileIOPermissionAccess.Read;

            title = Path.GetFileNameWithoutExtension(fileLoc);

            author = File.ReadAllText(fileLoc + "/author.cb");

            code = File.ReadAllText(fileLoc + "/code.cb");

            codeinfo = File.ReadAllText(fileLoc + "/codeinfo.cb");

            lang = File.ReadAllText(fileLoc + "/lang.cb");

            param = File.ReadAllText(fileLoc + "/param.cb");

            version = File.ReadAllText(fileLoc + "/version.cb");

            return true;
        }
    }
}
