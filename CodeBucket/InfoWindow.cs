﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class InfoWindow : UserControl
    {
        public InfoWindow()
        {
            InitializeComponent();
        }

        public void LoadBucket(Bucket bucket)
        {
            authorBox.Text = bucket.author;
            versionBox.Text = bucket.version;
            langBox.Text = bucket.lang;
            infoBox.Text = bucket.codeinfo;
        }
    }
}
