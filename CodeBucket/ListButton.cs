﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class ListButton : UserControl
    {

        public ListButton()
        {
            InitializeComponent();
        }

        [Browsable(true), 
            Category("Label"), 
            Description("Label on the button"),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Bindable(true)]
        public override string Text
        {
            get
            {
                return button4.Text;
            }
            set
            {
                button4.Text = value;
            }
        }

        [Browsable(true),
            Category("Text Align"),
            Description("Alignment of the text"),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Bindable(true)]
        public ContentAlignment textAlign
        {
            get
            {
                return button4.TextAlign;
            }
            set
            {
                button4.TextAlign = value;
            }
        }

        public event EventHandler ButtonClick;

        private void button4_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null)
                ButtonClick(this, e);
        }
    }
}