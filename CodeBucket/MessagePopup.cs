﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket
{
    public partial class MessagePopup : Form
    {
        public MessagePopup()
        {
            InitializeComponent();    
        }

        private void DrawArrow()
        {
           
        }

        private void MessagePopup_Load(object sender, EventArgs e)
        {

        }

        public void SetText(string str)
        {
            label1.Text = str;
        }

        public void SetLocation(int x, int y)
        {
            this.Location = new Point(x, y);
            this.Update();
        }
        public void SetLocation(Point pt)
        {
            this.Location = pt;
            this.Update();
        }

        private void MessagePopup_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            Point[] pointArray = new Point[3];

            pointArray[0] = new Point(22, 22);
            pointArray[1] = new Point(22, 52);
            pointArray[2] = new Point(4, 37);

            //g.DrawLine(new Pen(Brushes.Tan), new Point(panel2.Location.X, panel2.Location.Y + (panel2.Height / 2)), new Point(22, 22));

            g.FillPolygon(Brushes.AntiqueWhite, pointArray);

            Pen tanBrush = new Pen(Brushes.Tan);

            Point temp1 = pointArray[0];
            Point temp2 = pointArray[1];
            g.DrawLine(tanBrush, temp1.X, temp1.Y, temp2.X, temp2.Y);

            temp1 = pointArray[1];
            temp2 = pointArray[2];
            g.DrawLine(tanBrush, temp1.X, temp1.Y, temp2.X, temp2.Y);

            temp1 = pointArray[2];
            temp2 = pointArray[0];
            g.DrawLine(tanBrush, temp1.X, temp1.Y, temp2.X, temp2.Y);

            g.Dispose();
        }
    }
}
