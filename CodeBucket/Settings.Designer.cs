﻿namespace CodeBucket
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Label1 = new System.Windows.Forms.Label();
            this.Button13 = new System.Windows.Forms.Button();
            this.Panel63 = new System.Windows.Forms.Panel();
            this.Panel64 = new System.Windows.Forms.Panel();
            this.Panel62 = new System.Windows.Forms.Panel();
            this.Button1 = new System.Windows.Forms.Button();
            this.Panel12 = new System.Windows.Forms.Panel();
            this.Panel13 = new System.Windows.Forms.Panel();
            this.Panel11 = new System.Windows.Forms.Panel();
            this.Button2 = new System.Windows.Forms.Button();
            this.Panel15 = new System.Windows.Forms.Panel();
            this.Panel16 = new System.Windows.Forms.Panel();
            this.Panel14 = new System.Windows.Forms.Panel();
            this.Panel17 = new System.Windows.Forms.Panel();
            this.Button3 = new System.Windows.Forms.Button();
            this.Panel18 = new System.Windows.Forms.Panel();
            this.Panel19 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Panel7 = new System.Windows.Forms.Panel();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.Panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.Label9 = new System.Windows.Forms.Label();
            this.panel28 = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Panel62.SuspendLayout();
            this.Panel11.SuspendLayout();
            this.Panel14.SuspendLayout();
            this.Panel17.SuspendLayout();
            this.Panel2.SuspendLayout();
            this.Panel1.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel25.SuspendLayout();
            this.Panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 447);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(741, 1);
            this.Panel10.TabIndex = 131;
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.Label1.Location = new System.Drawing.Point(5, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(195, 28);
            this.Label1.TabIndex = 124;
            this.Label1.Text = "General";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Button13
            // 
            this.Button13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Button13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button13.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.Button13.FlatAppearance.BorderSize = 0;
            this.Button13.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.Button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button13.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Button13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Button13.Location = new System.Drawing.Point(0, 1);
            this.Button13.Name = "Button13";
            this.Button13.Size = new System.Drawing.Size(199, 25);
            this.Button13.TabIndex = 2;
            this.Button13.Text = "Buckets";
            this.Button13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button13.UseVisualStyleBackColor = false;
            // 
            // Panel63
            // 
            this.Panel63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel63.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel63.Location = new System.Drawing.Point(0, 26);
            this.Panel63.Name = "Panel63";
            this.Panel63.Size = new System.Drawing.Size(199, 1);
            this.Panel63.TabIndex = 4;
            // 
            // Panel64
            // 
            this.Panel64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel64.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel64.Location = new System.Drawing.Point(0, 0);
            this.Panel64.Name = "Panel64";
            this.Panel64.Size = new System.Drawing.Size(199, 1);
            this.Panel64.TabIndex = 3;
            // 
            // Panel62
            // 
            this.Panel62.Controls.Add(this.Button13);
            this.Panel62.Controls.Add(this.Panel63);
            this.Panel62.Controls.Add(this.Panel64);
            this.Panel62.Location = new System.Drawing.Point(1, 31);
            this.Panel62.Name = "Panel62";
            this.Panel62.Size = new System.Drawing.Size(199, 27);
            this.Panel62.TabIndex = 124;
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.Button1.FlatAppearance.BorderSize = 0;
            this.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Button1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Button1.Location = new System.Drawing.Point(0, 1);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(199, 25);
            this.Button1.TabIndex = 2;
            this.Button1.Text = "Design";
            this.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button1.UseVisualStyleBackColor = false;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Panel12
            // 
            this.Panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel12.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel12.Location = new System.Drawing.Point(0, 26);
            this.Panel12.Name = "Panel12";
            this.Panel12.Size = new System.Drawing.Size(199, 1);
            this.Panel12.TabIndex = 4;
            // 
            // Panel13
            // 
            this.Panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel13.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel13.Location = new System.Drawing.Point(0, 0);
            this.Panel13.Name = "Panel13";
            this.Panel13.Size = new System.Drawing.Size(199, 1);
            this.Panel13.TabIndex = 3;
            // 
            // Panel11
            // 
            this.Panel11.Controls.Add(this.Button1);
            this.Panel11.Controls.Add(this.Panel12);
            this.Panel11.Controls.Add(this.Panel13);
            this.Panel11.Location = new System.Drawing.Point(1, 57);
            this.Panel11.Name = "Panel11";
            this.Panel11.Size = new System.Drawing.Size(199, 27);
            this.Panel11.TabIndex = 125;
            // 
            // Button2
            // 
            this.Button2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.Button2.FlatAppearance.BorderSize = 0;
            this.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Button2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Button2.Location = new System.Drawing.Point(0, 1);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(199, 25);
            this.Button2.TabIndex = 2;
            this.Button2.Text = "Syntax Highlighting";
            this.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button2.UseVisualStyleBackColor = false;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Panel15
            // 
            this.Panel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel15.Location = new System.Drawing.Point(0, 26);
            this.Panel15.Name = "Panel15";
            this.Panel15.Size = new System.Drawing.Size(199, 1);
            this.Panel15.TabIndex = 4;
            // 
            // Panel16
            // 
            this.Panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel16.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel16.Location = new System.Drawing.Point(0, 0);
            this.Panel16.Name = "Panel16";
            this.Panel16.Size = new System.Drawing.Size(199, 1);
            this.Panel16.TabIndex = 3;
            // 
            // Panel14
            // 
            this.Panel14.Controls.Add(this.Button2);
            this.Panel14.Controls.Add(this.Panel15);
            this.Panel14.Controls.Add(this.Panel16);
            this.Panel14.Location = new System.Drawing.Point(1, 83);
            this.Panel14.Name = "Panel14";
            this.Panel14.Size = new System.Drawing.Size(199, 27);
            this.Panel14.TabIndex = 126;
            // 
            // Panel17
            // 
            this.Panel17.Controls.Add(this.Button3);
            this.Panel17.Controls.Add(this.Panel18);
            this.Panel17.Controls.Add(this.Panel19);
            this.Panel17.Location = new System.Drawing.Point(1, 292);
            this.Panel17.Name = "Panel17";
            this.Panel17.Size = new System.Drawing.Size(199, 27);
            this.Panel17.TabIndex = 128;
            // 
            // Button3
            // 
            this.Button3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.Button3.FlatAppearance.BorderSize = 0;
            this.Button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.Button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Button3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Button3.Location = new System.Drawing.Point(0, 1);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(199, 25);
            this.Button3.TabIndex = 2;
            this.Button3.Text = "Open Source Software";
            this.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button3.UseVisualStyleBackColor = false;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Panel18
            // 
            this.Panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel18.Location = new System.Drawing.Point(0, 26);
            this.Panel18.Name = "Panel18";
            this.Panel18.Size = new System.Drawing.Size(199, 1);
            this.Panel18.TabIndex = 4;
            // 
            // Panel19
            // 
            this.Panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel19.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel19.Location = new System.Drawing.Point(0, 0);
            this.Panel19.Name = "Panel19";
            this.Panel19.Size = new System.Drawing.Size(199, 1);
            this.Panel19.TabIndex = 3;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.Panel2.Controls.Add(this.Label6);
            this.Panel2.Controls.Add(this.Label5);
            this.Panel2.Controls.Add(this.Label7);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(741, 36);
            this.Panel2.TabIndex = 126;
            // 
            // Label6
            // 
            this.Label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label6.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label6.Location = new System.Drawing.Point(700, 5);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 27);
            this.Label6.TabIndex = 3;
            this.Label6.Text = "X";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label5
            // 
            this.Label5.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.Label5.ForeColor = System.Drawing.Color.Black;
            this.Label5.Location = new System.Drawing.Point(11, 5);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(296, 27);
            this.Label5.TabIndex = 1;
            this.Label5.Text = "CodeBucket Settings";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label7.Font = new System.Drawing.Font("Segoe UI Light", 22F);
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label7.Location = new System.Drawing.Point(658, 5);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(36, 27);
            this.Label7.TabIndex = 2;
            this.Label7.Text = "Ξ";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(741, 1);
            this.Panel7.TabIndex = 1;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(741, 1);
            this.Panel3.TabIndex = 125;
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.Panel1.Controls.Add(this.Panel5);
            this.Panel1.Controls.Add(this.Label1);
            this.Panel1.Controls.Add(this.label2);
            this.Panel1.Controls.Add(this.panel20);
            this.Panel1.Controls.Add(this.Label9);
            this.Panel1.Controls.Add(this.Panel17);
            this.Panel1.Controls.Add(this.Panel14);
            this.Panel1.Controls.Add(this.Panel11);
            this.Panel1.Controls.Add(this.Panel62);
            this.Panel1.Controls.Add(this.panel28);
            this.Panel1.Controls.Add(this.panel25);
            this.Panel1.Controls.Add(this.label3);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel1.Location = new System.Drawing.Point(1, 37);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(200, 383);
            this.Panel1.TabIndex = 129;
            // 
            // Panel5
            // 
            this.Panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel5.Location = new System.Drawing.Point(199, 0);
            this.Panel5.Name = "Panel5";
            this.Panel5.Size = new System.Drawing.Size(1, 383);
            this.Panel5.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.label2.Location = new System.Drawing.Point(10, 200);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 28);
            this.label2.TabIndex = 133;
            this.label2.Text = "Software";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.button4);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Controls.Add(this.panel22);
            this.panel20.Location = new System.Drawing.Point(1, 231);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(199, 27);
            this.panel20.TabIndex = 134;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button4.Location = new System.Drawing.Point(0, 1);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(199, 25);
            this.button4.TabIndex = 2;
            this.button4.Text = "Check for Updates";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel21.Location = new System.Drawing.Point(0, 26);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(199, 1);
            this.panel21.TabIndex = 4;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 0);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(199, 1);
            this.panel22.TabIndex = 3;
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.SystemColors.Control;
            this.Label9.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.Label9.Location = new System.Drawing.Point(10, 261);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(191, 28);
            this.Label9.TabIndex = 127;
            this.Label9.Text = "Regulatory";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.button6);
            this.panel28.Controls.Add(this.panel29);
            this.panel28.Controls.Add(this.panel30);
            this.panel28.Location = new System.Drawing.Point(1, 170);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(199, 27);
            this.panel28.TabIndex = 132;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button6.Location = new System.Drawing.Point(0, 1);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(199, 25);
            this.button6.TabIndex = 2;
            this.button6.Text = "Cloud Providers";
            this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel29.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel29.Location = new System.Drawing.Point(0, 26);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(199, 1);
            this.panel29.TabIndex = 4;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(199, 1);
            this.panel30.TabIndex = 3;
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.button5);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Controls.Add(this.panel27);
            this.panel25.Location = new System.Drawing.Point(1, 144);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(199, 27);
            this.panel25.TabIndex = 131;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.button5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(112)))), ((int)(((byte)(192)))), ((int)(((byte)(231)))));
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(184)))), ((int)(((byte)(224)))), ((int)(((byte)(243)))));
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(243)))), ((int)(((byte)(251)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.button5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.button5.Location = new System.Drawing.Point(0, 1);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(199, 25);
            this.button5.TabIndex = 2;
            this.button5.Text = "Local Backup";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel26.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel26.Location = new System.Drawing.Point(0, 26);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(199, 1);
            this.panel26.TabIndex = 4;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel27.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(199, 1);
            this.panel27.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.Control;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10.5F);
            this.label3.Location = new System.Drawing.Point(9, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 28);
            this.label3.TabIndex = 130;
            this.label3.Text = "Backup";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel6.Location = new System.Drawing.Point(0, 448);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(743, 1);
            this.Panel6.TabIndex = 127;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 448);
            this.Panel4.TabIndex = 124;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(742, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 448);
            this.Panel8.TabIndex = 128;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.SystemColors.Control;
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 420);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(741, 27);
            this.Panel9.TabIndex = 130;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(741, 1);
            this.panel23.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(592, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 132;
            this.label4.Text = "Show line numbers?";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(320, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(180, 13);
            this.label8.TabIndex = 133;
            this.label8.Text = "information alignment: left mid or right";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(743, 449);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Panel1);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Settings";
            this.Text = "Settings";
            this.Shown += new System.EventHandler(this.Settings_Shown);
            this.Panel62.ResumeLayout(false);
            this.Panel11.ResumeLayout(false);
            this.Panel14.ResumeLayout(false);
            this.Panel17.ResumeLayout(false);
            this.Panel2.ResumeLayout(false);
            this.Panel1.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel28.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.Panel9.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button Button13;
        internal System.Windows.Forms.Panel Panel63;
        internal System.Windows.Forms.Panel Panel64;
        internal System.Windows.Forms.Panel Panel62;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Panel Panel12;
        internal System.Windows.Forms.Panel Panel13;
        internal System.Windows.Forms.Panel Panel11;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Panel Panel15;
        internal System.Windows.Forms.Panel Panel16;
        internal System.Windows.Forms.Panel Panel14;
        internal System.Windows.Forms.Panel Panel17;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Panel Panel18;
        internal System.Windows.Forms.Panel Panel19;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Panel Panel7;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.Panel Panel5;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Panel Panel6;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Panel panel25;
        internal System.Windows.Forms.Button button5;
        internal System.Windows.Forms.Panel panel26;
        internal System.Windows.Forms.Panel panel27;
        internal System.Windows.Forms.Panel panel28;
        internal System.Windows.Forms.Button button6;
        internal System.Windows.Forms.Panel panel29;
        internal System.Windows.Forms.Panel panel30;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Panel panel20;
        internal System.Windows.Forms.Button button4;
        internal System.Windows.Forms.Panel panel21;
        internal System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
    }
}