﻿namespace CodeBucket
{
    partial class SendToDistributionLab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel7 = new System.Windows.Forms.Panel();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.Label6 = new System.Windows.Forms.Label();
            this.licenseName = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel3 = new System.Windows.Forms.Panel();
            this.Panel9 = new System.Windows.Forms.Panel();
            this.Panel10 = new System.Windows.Forms.Panel();
            this.Panel8 = new System.Windows.Forms.Panel();
            this.Panel4 = new System.Windows.Forms.Panel();
            this.Panel6 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Panel2.SuspendLayout();
            this.Panel9.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel7
            // 
            this.Panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel7.Location = new System.Drawing.Point(0, 35);
            this.Panel7.Name = "Panel7";
            this.Panel7.Size = new System.Drawing.Size(544, 1);
            this.Panel7.TabIndex = 1;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.Panel2.Controls.Add(this.Label6);
            this.Panel2.Controls.Add(this.licenseName);
            this.Panel2.Controls.Add(this.Panel7);
            this.Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel2.Location = new System.Drawing.Point(1, 1);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(544, 36);
            this.Panel2.TabIndex = 146;
            // 
            // Label6
            // 
            this.Label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label6.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.Label6.Location = new System.Drawing.Point(503, 5);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 27);
            this.Label6.TabIndex = 3;
            this.Label6.Text = "X";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // licenseName
            // 
            this.licenseName.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.licenseName.ForeColor = System.Drawing.Color.Black;
            this.licenseName.Location = new System.Drawing.Point(11, 5);
            this.licenseName.Name = "licenseName";
            this.licenseName.Size = new System.Drawing.Size(486, 27);
            this.licenseName.TabIndex = 1;
            this.licenseName.Text = "Piping to Distribution Lab";
            this.licenseName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel23.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(544, 1);
            this.panel23.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(12, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(522, 27);
            this.label1.TabIndex = 143;
            this.label1.Text = "Distribution Lab is not installed.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Panel3
            // 
            this.Panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel3.Location = new System.Drawing.Point(1, 0);
            this.Panel3.Name = "Panel3";
            this.Panel3.Size = new System.Drawing.Size(544, 1);
            this.Panel3.TabIndex = 145;
            // 
            // Panel9
            // 
            this.Panel9.BackColor = System.Drawing.SystemColors.Control;
            this.Panel9.Controls.Add(this.panel23);
            this.Panel9.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel9.Location = new System.Drawing.Point(1, 267);
            this.Panel9.Name = "Panel9";
            this.Panel9.Size = new System.Drawing.Size(544, 27);
            this.Panel9.TabIndex = 149;
            // 
            // Panel10
            // 
            this.Panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.Panel10.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel10.Location = new System.Drawing.Point(1, 294);
            this.Panel10.Name = "Panel10";
            this.Panel10.Size = new System.Drawing.Size(544, 1);
            this.Panel10.TabIndex = 150;
            // 
            // Panel8
            // 
            this.Panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel8.Location = new System.Drawing.Point(545, 0);
            this.Panel8.Name = "Panel8";
            this.Panel8.Size = new System.Drawing.Size(1, 295);
            this.Panel8.TabIndex = 148;
            // 
            // Panel4
            // 
            this.Panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel4.Location = new System.Drawing.Point(0, 0);
            this.Panel4.Name = "Panel4";
            this.Panel4.Size = new System.Drawing.Size(1, 295);
            this.Panel4.TabIndex = 144;
            // 
            // Panel6
            // 
            this.Panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel6.Location = new System.Drawing.Point(0, 295);
            this.Panel6.Name = "Panel6";
            this.Panel6.Size = new System.Drawing.Size(546, 1);
            this.Panel6.TabIndex = 147;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.panel13);
            this.panel1.Controls.Add(this.panel14);
            this.panel1.Controls.Add(this.panel12);
            this.panel1.Controls.Add(this.panel11);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Location = new System.Drawing.Point(84, 54);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(138, 158);
            this.panel1.TabIndex = 151;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(3, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 27);
            this.label2.TabIndex = 152;
            this.label2.Text = "CodeBucket";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel13.Location = new System.Drawing.Point(1, 156);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(136, 1);
            this.panel13.TabIndex = 152;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(1, 157);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(136, 1);
            this.panel14.TabIndex = 151;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(1, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(136, 1);
            this.panel12.TabIndex = 150;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(137, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1, 158);
            this.panel11.TabIndex = 149;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1, 158);
            this.panel5.TabIndex = 145;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label3);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Controls.Add(this.panel17);
            this.panel15.Controls.Add(this.panel18);
            this.panel15.Controls.Add(this.panel19);
            this.panel15.Controls.Add(this.panel20);
            this.panel15.Location = new System.Drawing.Point(324, 53);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(138, 158);
            this.panel15.TabIndex = 152;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(3, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(132, 27);
            this.label3.TabIndex = 152;
            this.label3.Text = "Distribution Lab";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel16.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel16.Location = new System.Drawing.Point(1, 156);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(136, 1);
            this.panel16.TabIndex = 152;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel17.Location = new System.Drawing.Point(1, 157);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(136, 1);
            this.panel17.TabIndex = 151;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel18.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel18.Location = new System.Drawing.Point(1, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(136, 1);
            this.panel18.TabIndex = 150;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel19.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel19.Location = new System.Drawing.Point(137, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1, 158);
            this.panel19.TabIndex = 149;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(162)))), ((int)(((byte)(162)))), ((int)(((byte)(162)))));
            this.panel20.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel20.Location = new System.Drawing.Point(0, 0);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(1, 158);
            this.panel20.TabIndex = 145;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::CodeBucket.Properties.Resources.Icon_Error;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(228, 99);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(90, 80);
            this.pictureBox1.TabIndex = 153;
            this.pictureBox1.TabStop = false;
            // 
            // SendToDistributionLab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 296);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Panel2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel3);
            this.Controls.Add(this.Panel9);
            this.Controls.Add(this.Panel10);
            this.Controls.Add(this.Panel8);
            this.Controls.Add(this.Panel4);
            this.Controls.Add(this.Panel6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "SendToDistributionLab";
            this.Text = "SendToDistributionLab";
            this.Shown += new System.EventHandler(this.SendToDistributionLab_Shown);
            this.Panel2.ResumeLayout(false);
            this.Panel9.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel Panel7;
        internal System.Windows.Forms.Panel Panel2;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label licenseName;
        internal System.Windows.Forms.Panel panel23;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Panel Panel3;
        internal System.Windows.Forms.Panel Panel9;
        internal System.Windows.Forms.Panel Panel10;
        internal System.Windows.Forms.Panel Panel8;
        internal System.Windows.Forms.Panel Panel4;
        internal System.Windows.Forms.Panel Panel6;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Panel panel13;
        internal System.Windows.Forms.Panel panel14;
        internal System.Windows.Forms.Panel panel12;
        internal System.Windows.Forms.Panel panel11;
        internal System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel15;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Panel panel16;
        internal System.Windows.Forms.Panel panel17;
        internal System.Windows.Forms.Panel panel18;
        internal System.Windows.Forms.Panel panel19;
        internal System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}