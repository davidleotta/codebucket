﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeBucket;

namespace CodeBucket
{
    public partial class CodeWindow : UserControl
    {
        public CodeWindow()
        {
            InitializeComponent();
        }

        public void RestructureContent()
        {
            //Resizes objects to fit inside window with no gaps in between

            throw new NotImplementedException();
        }

        public void LoadBucket(Bucket bucket)
        {
            if (bucket != null)
            {
                codePanel1.UpdateCode(bucket.code);

                paramBox.Text = bucket.param;

            }
        }
    }
}
