﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBucket
{
    class IOFailedOpenBucket : Exception
    {
        public IOFailedOpenBucket()
        {
        }

        public IOFailedOpenBucket(string message) : base(message)
        {

        }

        public IOFailedOpenBucket(string message, Exception inner) : base(message, inner)
        {

        }

    }
}
