﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBucket.Exceptions
{
    class IOBucketInvalid : Exception
    {
        
        public IOBucketInvalid()
        {
        }

        public IOBucketInvalid(string message) : base(message)
        {

        }

        public IOBucketInvalid(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
