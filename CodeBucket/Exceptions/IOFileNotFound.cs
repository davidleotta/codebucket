﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBucket
{
    class IOFileNotFound : Exception
    {

        public IOFileNotFound()
        {
        }

        public IOFileNotFound(string message) : base(message)
        {

        }

        public IOFileNotFound(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
