﻿namespace CodeBucket
{
    partial class Settings_OpenSource
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel24 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.openSourceButton1 = new CodeBucket.Settings_Forms.OpenSourceButton();
            this.openSourceButton2 = new CodeBucket.Settings_Forms.OpenSourceButton();
            this.SuspendLayout();
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel24.Location = new System.Drawing.Point(3, 42);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(535, 1);
            this.panel24.TabIndex = 133;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(535, 28);
            this.label2.TabIndex = 134;
            this.label2.Text = "Open Source Software";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label1.Location = new System.Drawing.Point(3, 348);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(535, 23);
            this.label1.TabIndex = 137;
            this.label1.Text = "Click an item to view its license.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openSourceButton1
            // 
            this.openSourceButton1.BackColor = System.Drawing.SystemColors.Control;
            this.openSourceButton1.Location = new System.Drawing.Point(3, 49);
            this.openSourceButton1.Name = "openSourceButton1";
            this.openSourceButton1.Size = new System.Drawing.Size(535, 27);
            this.openSourceButton1.TabIndex = 138;
            this.openSourceButton1.Text = "HTML-Renderer";
            // 
            // openSourceButton2
            // 
            this.openSourceButton2.BackColor = System.Drawing.SystemColors.Control;
            this.openSourceButton2.Location = new System.Drawing.Point(3, 82);
            this.openSourceButton2.Name = "openSourceButton2";
            this.openSourceButton2.Size = new System.Drawing.Size(535, 27);
            this.openSourceButton2.TabIndex = 139;
            this.openSourceButton2.Text = "Icons8";
            // 
            // Settings_OpenSource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.openSourceButton2);
            this.Controls.Add(this.openSourceButton1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.label2);
            this.Name = "Settings_OpenSource";
            this.Size = new System.Drawing.Size(541, 383);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panel24;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Settings_Forms.OpenSourceButton openSourceButton1;
        private Settings_Forms.OpenSourceButton openSourceButton2;
    }
}
