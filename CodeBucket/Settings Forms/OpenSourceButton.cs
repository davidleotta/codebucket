﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeBucket.Settings_Forms
{
    public partial class OpenSourceButton : UserControl
    {

        ViewLicense vl;
        public OpenSourceButton()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            vl = new ViewLicense();
            vl.Show();
            vl.LoadLicense(button.Text);
        }

        [Browsable(true),
            Category("Label"),
            Description("Label on the button"),
            EditorBrowsable(EditorBrowsableState.Always),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            Bindable(true)]
        public override string Text
        {
            get
            {
                return button.Text;
            }
            set
            {
                button.Text = value;
            }
        }
    }
}
