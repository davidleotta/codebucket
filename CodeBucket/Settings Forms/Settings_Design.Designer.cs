﻿namespace CodeBucket
{
    partial class Settings_Design
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel24 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.listButton1 = new CodeBucket.ListButton();
            this.listButton2 = new CodeBucket.ListButton();
            this.listButton3 = new CodeBucket.ListButton();
            this.listButton4 = new CodeBucket.ListButton();
            this.listButton5 = new CodeBucket.ListButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.panel24.Location = new System.Drawing.Point(6, 42);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(535, 1);
            this.panel24.TabIndex = 140;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.5F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(535, 28);
            this.label2.TabIndex = 141;
            this.label2.Text = "Design";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // listButton1
            // 
            this.listButton1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton1.Location = new System.Drawing.Point(179, 69);
            this.listButton1.Name = "listButton1";
            this.listButton1.Size = new System.Drawing.Size(183, 28);
            this.listButton1.TabIndex = 142;
            this.listButton1.Text = "Light";
            this.listButton1.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listButton2
            // 
            this.listButton2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton2.Location = new System.Drawing.Point(179, 99);
            this.listButton2.Name = "listButton2";
            this.listButton2.Size = new System.Drawing.Size(183, 28);
            this.listButton2.TabIndex = 143;
            this.listButton2.Text = "Dark";
            this.listButton2.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listButton3
            // 
            this.listButton3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton3.Location = new System.Drawing.Point(179, 129);
            this.listButton3.Name = "listButton3";
            this.listButton3.Size = new System.Drawing.Size(183, 28);
            this.listButton3.TabIndex = 144;
            this.listButton3.Text = "My Windows Theme";
            this.listButton3.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listButton4
            // 
            this.listButton4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton4.Location = new System.Drawing.Point(179, 347);
            this.listButton4.Name = "listButton4";
            this.listButton4.Size = new System.Drawing.Size(183, 28);
            this.listButton4.TabIndex = 146;
            this.listButton4.Text = "Import Theme";
            this.listButton4.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listButton5
            // 
            this.listButton5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listButton5.Location = new System.Drawing.Point(179, 317);
            this.listButton5.Name = "listButton5";
            this.listButton5.Size = new System.Drawing.Size(183, 28);
            this.listButton5.TabIndex = 145;
            this.listButton5.Text = "Custom Colour";
            this.listButton5.textAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.Location = new System.Drawing.Point(368, 331);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(26, 1);
            this.panel7.TabIndex = 153;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Gainsboro;
            this.panel8.Location = new System.Drawing.Point(368, 361);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(26, 1);
            this.panel8.TabIndex = 154;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.Location = new System.Drawing.Point(147, 361);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(26, 1);
            this.panel9.TabIndex = 156;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Gainsboro;
            this.panel10.Location = new System.Drawing.Point(147, 331);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(26, 1);
            this.panel10.TabIndex = 155;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Gainsboro;
            this.panel11.Location = new System.Drawing.Point(146, 331);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(1, 31);
            this.panel11.TabIndex = 157;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Gainsboro;
            this.panel12.Location = new System.Drawing.Point(393, 331);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1, 31);
            this.panel12.TabIndex = 158;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Gainsboro;
            this.panel13.Location = new System.Drawing.Point(394, 346);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(113, 1);
            this.panel13.TabIndex = 159;
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Gainsboro;
            this.panel14.Location = new System.Drawing.Point(34, 347);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(113, 1);
            this.panel14.TabIndex = 160;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.Location = new System.Drawing.Point(368, 113);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(26, 1);
            this.panel1.TabIndex = 163;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Location = new System.Drawing.Point(368, 83);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(26, 1);
            this.panel2.TabIndex = 162;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Gainsboro;
            this.panel3.Location = new System.Drawing.Point(368, 143);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(26, 1);
            this.panel3.TabIndex = 164;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.DimGray;
            this.panel16.Location = new System.Drawing.Point(394, 83);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(1, 31);
            this.panel16.TabIndex = 165;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DimGray;
            this.panel4.Location = new System.Drawing.Point(146, 83);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1, 31);
            this.panel4.TabIndex = 169;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gainsboro;
            this.panel5.Location = new System.Drawing.Point(146, 143);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(26, 1);
            this.panel5.TabIndex = 168;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Gainsboro;
            this.panel6.Location = new System.Drawing.Point(146, 113);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(26, 1);
            this.panel6.TabIndex = 167;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.DimGray;
            this.panel17.Location = new System.Drawing.Point(146, 83);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(26, 1);
            this.panel17.TabIndex = 166;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.DimGray;
            this.panel15.Location = new System.Drawing.Point(394, 113);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(113, 1);
            this.panel15.TabIndex = 170;
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.DimGray;
            this.panel18.Location = new System.Drawing.Point(34, 113);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(113, 1);
            this.panel18.TabIndex = 171;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.DimGray;
            this.panel19.Location = new System.Drawing.Point(34, 113);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(1, 114);
            this.panel19.TabIndex = 172;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.DimGray;
            this.panel20.Location = new System.Drawing.Point(506, 113);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(1, 114);
            this.panel20.TabIndex = 173;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.DimGray;
            this.panel21.Location = new System.Drawing.Point(368, 226);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(139, 1);
            this.panel21.TabIndex = 174;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.DimGray;
            this.panel22.Location = new System.Drawing.Point(34, 226);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(139, 1);
            this.panel22.TabIndex = 175;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.label1.Location = new System.Drawing.Point(179, 212);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(183, 28);
            this.label1.TabIndex = 176;
            this.label1.Text = "Light";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Gainsboro;
            this.panel23.Location = new System.Drawing.Point(394, 113);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(1, 31);
            this.panel23.TabIndex = 177;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.Color.Gainsboro;
            this.panel25.Location = new System.Drawing.Point(146, 113);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(1, 31);
            this.panel25.TabIndex = 178;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Gainsboro;
            this.panel26.Location = new System.Drawing.Point(506, 227);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(1, 119);
            this.panel26.TabIndex = 179;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.Color.Gainsboro;
            this.panel27.Location = new System.Drawing.Point(34, 227);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(1, 120);
            this.panel27.TabIndex = 180;
            // 
            // Settings_Design
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.panel27);
            this.Controls.Add(this.panel26);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel22);
            this.Controls.Add(this.panel21);
            this.Controls.Add(this.panel20);
            this.Controls.Add(this.panel19);
            this.Controls.Add(this.panel18);
            this.Controls.Add(this.panel15);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel13);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.listButton4);
            this.Controls.Add(this.listButton5);
            this.Controls.Add(this.listButton3);
            this.Controls.Add(this.listButton2);
            this.Controls.Add(this.listButton1);
            this.Controls.Add(this.panel24);
            this.Controls.Add(this.label2);
            this.Name = "Settings_Design";
            this.Size = new System.Drawing.Size(541, 383);
            this.Load += new System.EventHandler(this.Settings_Design_Load);
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.Panel panel24;
        internal System.Windows.Forms.Label label2;
        private ListButton listButton1;
        private ListButton listButton2;
        private ListButton listButton3;
        private ListButton listButton4;
        private ListButton listButton5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
    }
}
