﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CodeBucket
{
    public partial class CodePanel : UserControl
    {

        CodeEditor codeEditor;

        public CodePanel()
        {
            InitializeComponent();
        }

        private void CodePanel_Load(object sender, EventArgs e)
        {
            //richTextBox1.Rtf = @"{\rtf1\ansi This text is in \b bold\b0, " +
            //@"this is in \i italics\i, " +
            //@"and this is \ul underlined\ul0.}";

            addExampleText();

            var syntax = Regex.Escape("private");
            foreach (Match match in Regex.Matches(richTextBox1.Text, syntax))
            {
                richTextBox1.Select(match.Index, syntax.Length);
                richTextBox1.SelectionColor = Color.Aqua;
                richTextBox1.Select(richTextBox1.TextLength, 0);
                richTextBox1.SelectionColor = richTextBox1.ForeColor;
            }
        }

        private void addExampleText()
        {

        }

        public void ProcessHTML(string str)
        {

        }

        private void CodePanel_TextChanged(object sender, EventArgs e)
        {
            HighlightSyntax();
        }

        private void HighlightKeyword(int start, int finish)
        {
            richTextBox1.Select(start, finish);
            richTextBox1.SelectionColor = Color.Blue;
            richTextBox1.Select(richTextBox1.TextLength, 0);
            richTextBox1.SelectionColor = richTextBox1.ForeColor;
        }

        private void HighlightDataType(int start, int finish)
        {
            richTextBox1.Select(start, finish);
            richTextBox1.SelectionColor = Color.Aqua;
            richTextBox1.Select(richTextBox1.TextLength, 0);
            richTextBox1.SelectionColor = richTextBox1.ForeColor;
        }

        private void HighlightBool(int start, int finish)
        {
            richTextBox1.Select(start, finish);
            richTextBox1.SelectionColor = Color.GreenYellow;
            richTextBox1.Select(richTextBox1.TextLength, 0);
            richTextBox1.SelectionColor = richTextBox1.ForeColor;
        }

        private void EditCode()
        {
            if (codeEditor == null)
                codeEditor = new CodeEditor(this);

            codeEditor.Show();

            codeEditor.SetCode(richTextBox1.Text);

            
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            EditCode();
        }

        public void UpdateCode(string code)
        {
            richTextBox1.Text = code;

            RemoveEmptyLines();

            //Trigger save function

            //Also trigger RestructureContent()
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            CopyCode();
        }

        /// <summary>
        /// Copies the entire code into system clipboard
        /// method simulates system select all + copy
        /// </summary>
        private void CopyCode()
        {
            //Clipboard.SetText doesnt maintain new lines
            //for now just dirty copy, but find a new solution

            richTextBox1.SelectAll();
            richTextBox1.Copy();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditCode();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyCode();
        }

        private void RemoveEmptyLines()
        {
            richTextBox1.Text = Regex.Replace(richTextBox1.Text, @"^\s*$(\n|\r|\r\n)", "", RegexOptions.Multiline);
        }

        private void HighlightSyntax()
        {
            for (int i = 0; i < SyntaxHighlighting.keywords.Count; i++)
            {
                var syntax = Regex.Escape(SyntaxHighlighting.keywords[i].ToString());
                foreach (Match match in Regex.Matches(richTextBox1.Text, syntax))
                {
                    HighlightKeyword(match.Index, syntax.Length);
                }
            }

            for (int i = 0; i < SyntaxHighlighting.datatypes.Count; i++)
            {
                var syntax = Regex.Escape(SyntaxHighlighting.datatypes[i].ToString());
                foreach (Match match in Regex.Matches(richTextBox1.Text, syntax))
                {
                    HighlightDataType(match.Index, syntax.Length);
                }
            }

            for (int i = 0; i < SyntaxHighlighting.bools.Count; i++)
            {
                var syntax = Regex.Escape(SyntaxHighlighting.bools[i].ToString());
                foreach (Match match in Regex.Matches(richTextBox1.Text, syntax))
                {
                    HighlightBool(match.Index, syntax.Length);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //temp reanalyse button
            HighlightSyntax();
        }
    }
}

